<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Images Controller
 *
 *
 * @method \App\Model\Entity\Image[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ImagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $images = $this->paginate($this->Images);
        $this->set(compact('images'));
    }


    /**
     * Function which uploads and resize image
     */
    public function upload()
    {
        if (!empty($_FILES) && isset($_FILES['document']['tmp_name']) && !empty($_FILES['document']['tmp_name'])) {

            //Getting document
            $tmpFilePath = $_FILES['document']['tmp_name'];
            $result = $this->Images->processImageFile($tmpFilePath);

            if($result) {
//                print_r($result);exit;
                $this->Flash->success(__('Image Process Successful'));
                $this->set('imageData', $result);
                $this->render('view');
            } else {
                $this->Flash->error(__('Failed to process image'));
                $this->redirect('/');
            }
        } else {
            $this->Flash->error(__('Please upload image'));
            $this->redirect('/');
        }
    }

}
