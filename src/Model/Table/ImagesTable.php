<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Images Model
 *
 * @method \App\Model\Entity\Image get($primaryKey, $options = [])
 * @method \App\Model\Entity\Image newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Image[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Image|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Image patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Image[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Image findOrCreate($search, callable $callback = null, $options = [])
 */
class ImagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('images');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('url')
            ->maxLength('url', 512)
            ->requirePresence('url', 'create')
            ->notEmpty('url');

        $validator
            ->scalar('url_640')
            ->maxLength('url_640', 512)
            ->requirePresence('url_640', 'create')
            ->notEmpty('url_640');

        $validator
            ->scalar('url_1280')
            ->maxLength('url_1280', 512)
            ->requirePresence('url_1280', 'create')
            ->notEmpty('url_1280');

        $validator
            ->dateTime('created_on')
            ->allowEmpty('created_on');

        return $validator;
    }



    /**
     * @param $tmpFilePath
     * @return bool
     */

    public function processImageFile($tmpFilePath) {

        // Image Type
        $type = exif_imagetype($tmpFilePath);
        if ($type) {
            // Uploaded File Extension
            $extension = image_type_to_extension($type);
            $imageData = array();
            // Original File Name
            $originalFileName = 'image_' . date('YmdHis') . "_original" . $extension;

            //Storing original image
            $fileUploadResult = $this->uploadFile($tmpFilePath, $originalFileName);
            $imageData['url'] = $fileUploadResult;

            // Cropped 1280x720 File Name
            // Extension will always be png
            $largeFileName = 'image_' . date('YmdHis') . "_1280.png";
            $largeFileResult = $this->copyFile($fileUploadResult, $largeFileName);
            $this->resizeImage($largeFileResult, $type, 1280, 720);
            $imageData['url_1280'] = $largeFileResult;

            // Cropped 640x480 File Name
            $smallFileName = 'image_' . date('YmdHis') . "_640.png";
            $smallFileResult = $this->copyFile( $fileUploadResult, $smallFileName);
            $this->resizeImage($smallFileResult, $type, 640, 480, false); // False consider  aspect ratio
            $imageData['url_640'] = $smallFileResult;

            //Entry to DB
            $imageResult = $this->newEntity($imageData);
            $storedData = $this->save($imageResult);
            if($storedData){
                return $storedData;
            }
        }
        return false;
    }


    /**
     * @param $filePath
     * @param $newFileName
     * @return bool|string
     *
     * Function which copy file
     */

    public function copyFile($filePath, $newFileName) {
        // Upload Folder
        $uploadFolder = WWW_ROOT;
        $storingPath = "uploads/";
        $filePath = WWW_ROOT . $filePath;

        // Check file exists
        if (file_exists($uploadFolder . $storingPath . $newFileName)) {
            $fileCounter = 0;
            while (file_exists($uploadFolder . $storingPath . $fileCounter . "_" . $newFileName)) {
                $fileCounter++;
            }

            $newFilePath = $uploadFolder . $storingPath . $fileCounter . "_" . $newFileName;
            $fileRelativeUrl = $storingPath . $fileCounter . "_" . $newFileName;
        } else {
            $newFilePath = $uploadFolder . $storingPath . $newFileName;
            $fileRelativeUrl = $storingPath . $newFileName;
        }

        if ($filePath && file_exists($filePath)) {
            copy($filePath, $newFilePath);
            return $fileRelativeUrl;
        }
        return false;
    }


    /**
     * @param $tmpFilePath
     * @param $fileName
     * @param string $storingPath
     * @return string
     *
     * Function to save file in server
     */

    public function uploadFile($tmpFilePath, $fileName, $storingPath = "") {
        // Upload Folder
        $uploadFolder = WWW_ROOT;
        if (empty($storingPath)) {
            $storingPath = "uploads/";
        }

        if (!file_exists($uploadFolder . $storingPath)) {
            mkdir($uploadFolder . $storingPath);
        }
        // Check file exists
        if (file_exists($uploadFolder . $storingPath . $fileName)) {
            $fileCounter = 0;
            while (file_exists($uploadFolder . $storingPath . $fileCounter . "_" . $fileName)) {
                $fileCounter++;
            }

            $newFilePath = $uploadFolder . $storingPath . $fileCounter . "_" . $fileName;
            $fileRelativeUrl = $storingPath . $fileCounter . "_" . $fileName;
        } else {
            $newFilePath = $uploadFolder . $storingPath . $fileName;
            $fileRelativeUrl = $storingPath . $fileName;
        }

        //Make sure we have a filepath
        if ($tmpFilePath != "") {
            if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                //Returning relative url
                return $fileRelativeUrl;
            }
        }
    }

    /**
     * @param $originalFile
     * @param $type
     * @param $desWidth
     * @param $desHeight
     * @param bool $keepAspectRatio
     * @return bool
     *
     *
     */
    public function resizeImage($originalFile, $type, $desWidth, $desHeight, $keepAspectRatio = false) {
        $originalFile = WWW_ROOT . $originalFile;
        if(!empty($originalFile) && !empty($type)) {
            switch ($type) {
                case IMAGETYPE_GIF:
                    $originalFileImageSource = imagecreatefromgif($originalFile);
                    break;

                case IMAGETYPE_JPEG:
                    $originalFileImageSource = imagecreatefromjpeg($originalFile);
                    break;

                case IMAGETYPE_PNG:
                    $originalFileImageSource = imagecreatefrompng($originalFile);
                    break;
            }

            if ($originalFileImageSource) {
                $size = getimagesize($originalFile);
                $naturalWidth = $size[0]; // natural width
                $naturalHeight = $size[1]; // natural height

                //To keep default
                $newWidth = $desWidth;
                $newHeight = $desHeight;

                //Consider Aspect ration of image
                if($keepAspectRatio) {
                    $ration = $naturalWidth / $naturalHeight;
                    if ($desWidth / $desHeight > $ration) {
                        $newWidth = $desHeight * $ration;
                        $newHeight = $desHeight;
                    } else {
                        $newHeight = $desWidth / $ration;
                        $newWidth = $desWidth;
                    }
                }

                $dst = imagecreatetruecolor($newWidth, $newHeight);
                $result = imagecopyresampled($dst, $originalFileImageSource, 0, 0, 0, 0, $newWidth, $newHeight, $naturalWidth, $naturalHeight);
                imagepng($dst,$originalFile);
                if($result) {
                    return true;
                }
            }
        }
        return false;
    }
}