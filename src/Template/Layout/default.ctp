<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$description = 'Welcome, Upload Image And We Will Resize Your Image';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    $title = 'Image Resizer';
    $description = 'Welcome, Upload Image And We Will Resize Your Image';
    $canonicalUrl = '/';
    ?>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title; ?></title>
    <link rel="canonical" href="<?= $canonicalUrl; ?>">

    <meta name="description" content="<?= $description; ?>">
    <?= $this->Html->meta('favicon.ico', 'https://getbootstrap.com/assets/img/favicons/favicon-32x32.png', ['type' => 'icon']); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">

    <!-- REQUIRED JS SCRIPTS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
</head>
<body>
    <div class="wrapper">
        <?= $this->fetch('content') ?>
    </div><!-- /.wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
<script src="/js/script.js"></script>
<?= $this->fetch('script') ?>
<script>
    $(function() {
        $("img.lazy").lazyload({
            threshold : 50
        });
    });
</script>
</body>
</html>
