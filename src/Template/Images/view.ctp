<section class="view">
    <div class="container">

        <?php
        if(isset($imageData) && !empty($imageData)) {
            ?>
            <div class="row">
                <div class="heading">
                    <h2>Processed Image   <a href="/all_images" class="btn btn-primary">View All Images</a></h2>
                </div>
            </div>
            <div class="row">
                <h4>
                    Original Image
                </h4>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="image-box centre">
                        <img src="<?= '/' . $imageData['url'] ?>" class="img-fluid" alt="Original Image">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <h4>
                        1280*720 Image
                    </h4>
                    <div class="image-box centre">
                        <img src="<?= '/' . $imageData['url_1280'] ?>" class="img-fluid" alt="1280 Image">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <h4>
                        640*480 Image
                    </h4>
                    <div class="image-box centre">
                        <img src="<?= '/' . $imageData['url_640'] ?>" class="img-fluid img-responsive" alt="640 Image">
                    </div>
                </div>
            </div>
        <?php
        }?>
    </div>
</section>