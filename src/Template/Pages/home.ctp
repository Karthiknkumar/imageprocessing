<!-- Submitted to Feature  March 2, 2014  11:20pm -->
<section id="home">
    <div class="container">
        <div class="page-holder">
            <div class="row">
                <h2>Welcome,</h2>
                <h3>Upload Image, We Will resize Image for you!! </h3>
            </div>
            <div class="row">
                <form action="/upload"  method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" name="document" class="custom-file-input" id="customFile" accept="image/*" required>
                            <label class="custom-file-label btn-outline-primary" for="customFile" >Choose file</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                </form>
            </div>
            <div class="row">
                <a href="/all_images" class="btn btn-primary">View All Images</a>
            </div>
        </div>
    </div>
</section>